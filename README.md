# Plantek PHPCS Wordpress Standards 
Coding Standard for PHPCS Designed for use with Plantek Wordpress Plugins & Themes.

## Installation via Composer


## Running
You can use the installed standard like:

```bash 
phpcs --standard=ptkwpphpstandards path/to/code
```
Alternatively if it isn't installed you can still reference it by path like:

```bash
phpcs --standard=path/to/plantek/ptkwpphpstandards path/to/code
```
